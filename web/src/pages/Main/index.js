import React,{ Component } from "react";
import MapGL, { Marker } from "react-map-gl";
import {Button} from 'reactstrap';
import Tutorial from '../../components/Tutorial';
import SideBar from '../../components/SideBar';
import ModalForm from '../../components/ModalForm';
import 'bootstrap/dist/css/bootstrap.min.css';
import "mapbox-gl/dist/mapbox-gl.css";
import {connect} from 'react-redux';
import Mark from '../../components/Mark';
import * as pointActions from '../../store/modules/points/actions';
import {bindActionCreators} from 'redux';

const buttonStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  padding: '10px'
};

const buttonSairStyle = {
  position: 'absolute',
  top: 0,
  left: 150,
  padding: '10px'
};

class Main extends Component {

  state = {
    viewport: {
      width: window.innerWidth,
      height: window.innerHeight,
      latitude: -30.0277,
      longitude:-51.2287,
      zoom: 14
    }
  };

  async componentDidMount() {
    window.addEventListener("resize", this._resize);
    this._resize();

    const {ListPointsRequest, match} = this.props

    ListPointsRequest(match.params.id)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this._resize);
  }

  _resize = () => {
    this.setState({
      viewport: {
        ...this.state.viewport,
        width: window.innerWidth,
        height: window.innerHeight
      }
    });
  };

  _onViewportChange = viewport => {
    this.setState({viewport});
  };

  handleMapClick = (e) => {
    const [latitude, longitude] = e.lngLat;

    const {OpenModalForm, match} = this.props;

    OpenModalForm( latitude, longitude, match.params.id)


  }

  render() {

    const { markers} =  this.props;

    return (
      <>
        <MapGL
          {...this.state.viewport}
          onClick={this.handleMapClick}
          mapStyle="mapbox://styles/mapbox/basic-v9"
          mapboxApiAccessToken={
            "pk.eyJ1IjoiZGllZ28zZyIsImEiOiJjamh0aHc4em0wZHdvM2tyc3hqbzNvanhrIn0.3HWnXHy_RCi35opzKo8sHQ"
          }
          onViewportChange={this._onViewportChange}
          >

          {markers &&  markers.map(mark => (
            <Marker
              key= {mark.id}
              //Converte a coordenada em valor numérico e substitui vírgula por ponto
              latitude={Number(mark.latitude.replace(',', '.'))  }
              longitude={Number(mark.longitude.replace(',', '.')) }
              onClick={this.handleMapClick}
              captureClick={true}
            >

            <Mark url = {mark.url} content = {mark.content}/>
          </Marker>
          ))}
        </MapGL>
        <div style= {buttonStyle}>
          <Tutorial />
        </div>
        <SideBar idUser = {this.props.match.params.id}/>
        <div style= {buttonSairStyle}>
          <Button onClick = {() => {this.props.history.push('/')}} className = "btn btn-danger">Sair</Button>
        </div>
        <ModalForm />
      </>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(pointActions, dispatch);

const mapStateToProps = state => ({
    lat: state.points.latitude,
    long: state.points.longitude,
    markers: state.points.markers
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
