import styled from 'styled-components';

const Container = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #f2f2f2;
`;

const Form = styled.form`
  width: 250px;
  display: flex;
  flex-direction: column;

  input {
    height: 40px;
    border: 1px solid #fd5c63;
    border-radius: 4px;
    font-size: 16px;
    padding: 0 20px;
    margin-top: 30px;
  }

  button {
    height: 40px;
    background: #fd5c63;
    border-radius: 4px;
    font-size: 16px;
    margin-top: 10px;
    color: #fff;
    font-weight: bold;
    cursor: pointer;
    border: 0;

    :hover {
      opacity: 0.7;
    }
  }
`;

export { Container, Form };
