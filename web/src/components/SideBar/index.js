import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Truncate from 'react-truncate';
import { formatDistance, parseISO } from 'date-fns';
import pt from 'date-fns/locale/pt';
import { Container } from './styles'
import * as pointActions from '../../store/modules/points/actions';
import {bindActionCreators} from 'redux';

function SideBar({ markers, RemovePointRequest, idUser }) {

  return (
    <Container>
      <h1>Marcadores Adicionados</h1>
      <ul>
        {markers.map(mark => (
          <li key={mark.id}>
            <div className="image">
              <img src={mark.url} alt="avatar" />
            </div>
            <div className="content" >
              <Truncate lines={1} width={150}>
                {mark.content}
              </Truncate>
              <span className="date">
                há{' '}
                {formatDistance(parseISO(mark.createdAt), new Date(), {
                  locale: pt,
                })}
              </span>
            </div>
            <div className="button">
              <button onClick = {() => RemovePointRequest(idUser, mark.id) } type="button">
                <i className="fa fa-fw fa-times-circle remove" />
              </button>
            </div>
          </li>
        ))}
      </ul>
    </Container>
  );
}

SideBar.propTypes = {
  markers: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,

  idUser: PropTypes.string.isRequired,

};

const mapStateToProps = state => ({
  markers: state.points.markers,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(pointActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
