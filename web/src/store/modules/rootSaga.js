import {all} from 'redux-saga/effects';
import points from './points/sagas';

export default function* rootSaga(){
  return yield all([
    points
  ])
}
