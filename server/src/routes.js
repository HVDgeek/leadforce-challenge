const { Router } = require('express');
const multer = require('multer');
const UserController = require('./app/controllers/UserController');
const PointController = require('./app/controllers/PointController');
const multerConfig = require('./config/multer');

const routes = new Router();
const upload = multer(multerConfig);

routes.post('/users', UserController.store);
routes.get('/users/:id', UserController.show);
routes.post('/users/:id/points', upload.single('file'), PointController.store);
routes.delete('/users/:id/points', PointController.delete);

module.exports = routes;
