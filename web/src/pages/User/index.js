import React, { useState } from 'react';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import logo from '../../assets/logo.svg';
import { Container, Form } from './styles';
import api from '../../services/api';

export default function User({ history }) {
  const [inputValue, setInputValue] = useState('');
  const [loading, setLoading] = useState(false);

  const handleChange = e => {
    setInputValue(e.target.value);
  };

  const handleSumit = async e => {
    e.preventDefault();

    try {
      setLoading(true);
      const response = await api.post('/users', {
        name: inputValue,
      });

      history.push(`/map/${response.data._id}`);

      toast.success(`${response.data.name}, Seja Bem Vindo ao Lead Map`);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      toast.error('Não foi possível entrar na aplicação');
    }
  };

  return (
    <Container>
      <Form onSubmit={handleSumit}>
        <img src={logo} alt="Lead Map" />
        <input
          value={inputValue}
          onChange={handleChange}
          placeholder="Your name"
          type="text"
        />
        <button type="submit">
          {loading ? <i className="fa fa-spinner fa-puse" /> : 'Entrar'}
        </button>
      </Form>
    </Container>
  );
}

User.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
