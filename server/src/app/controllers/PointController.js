const Point = require('../models/Point');
const User = require('../models/User');

class PointController {
  async store(req, res) {
    const { id } = req.params;
    try {
      const user = await User.findById(id);
      const { latitude, longitude, content } = req.body;
      const { filename: path, originalname: name } = req.file;

      const point = await Point.create({
        name,
        path,
        latitude,
        longitude,
        content,
      });

      user.points.push(point);
      await user.save();

      req.io.sockets.in(user._id).emit('point', point);

      return res.json(point);
    } catch (error) {
      return res.status(400).json({ error: 'user not found' });
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    const { idPoint } = req.query;

    try {
      const user = await User.findById(id);

      const point = user.points.find(item => item == idPoint);

      if (!point) {
        return res.status(400).json({ error: 'this point not exist' });
      }

      user.points = user.points.filter(item => item !== point);

      await user.save();

      return res.json(user.points);
    } catch (error) {
      return res.status(400).json({ error: 'user not found' });
    }
  }
}

module.exports = new PointController();
