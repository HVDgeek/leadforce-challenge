import React, { useState, useCallback, useEffect } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Input,
  Label,
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from './styles';
import * as pointActions from '../../store/modules/points/actions';

function ModalForm({ activeModal, latitude, longitude, CloseModalForm, id, AddPointRequest }) {
  const [modal, setModal] = useState(activeModal);
  const [newLat, setLat] = useState(0);
  const [newLong, setLong] = useState(0);
  const [newMsg, setMsg] = useState('');
  const [image, setImage] = useState('');

  const toggle = useCallback(() => {
    CloseModalForm();

    setModal(!activeModal);
  }, [CloseModalForm, activeModal]);

  function handleSubmit(e) {
    e.preventDefault();

    setLat(e.target.latitude.value);
    setLong(e.target.longitude.value);

    const data = new FormData();

    data.append('file', image);
    data.append('latitude', newLat.toString());
    data.append('longitude', newLong.toString());
    data.append('content', newMsg);

    AddPointRequest(id, data)

    setMsg('');
    toggle();
  }

  function handleImage(e) {
    setImage(e.target.files[0]);
  }

  useEffect(() => {
    setModal(activeModal);
    setLat(latitude);
    setLong(longitude);
  }, [activeModal, latitude, longitude]);

  return (
    <Container>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Adicionar Marcador no Mapa</ModalHeader>
        <Form className="form-plan" onSubmit={handleSubmit}>
          <ModalBody>
            <Input
              type="file"
              name="file"
              id="exampleFile"
              onChange={handleImage}
              required
            />
            <br />
            <Label>
              <strong>Latitude:</strong>
            </Label>
            <Input
              name="latitude"
              type="number"
              step={0.01}
              required
              placeholder="Latitude"
              onChange={e => setLat(e.target.value)}
              value={newLat}
            />
            <br />
            <Label>
              <strong>Longitude:</strong>
            </Label>
            <Input
              name="longitude"
              type="number"
              step={0.01}
              required
              placeholder="Longitude"
              onChange={e => setLong(e.target.value)}
              value={newLong}
            />
            <br />
            <Label>
              <strong>Mensagem:</strong>
            </Label>
            <Input
              type="text"
              required
              name="content"
              maxLength="30"
              id="TextArea"
              onChange={e => setMsg(e.target.value)}
              value={newMsg}
            />
          </ModalBody>
          <ModalFooter>
            <Button color="primary" type="submit">
              Adicionar
            </Button>{' '}
            <Button color="secondary" onClick={toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Form>
      </Modal>
    </Container>
  );
}

ModalForm.propTypes = {
  activeModal: PropTypes.bool.isRequired,
  CloseModalForm: PropTypes.func.isRequired,
  latitude: PropTypes.number.isRequired,
  longitude: PropTypes.number.isRequired,
  id: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  activeModal: state.points.activeModal,
  latitude: state.points.latitude,
  longitude: state.points.longitude,
  id: state.points.id,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(pointActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ModalForm);
