import React, { useState } from 'react';
import { Collapse, Button } from 'reactstrap';
import PropTypes from 'prop-types';

const Mark = ({ url, content }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Button
        onClick={toggle}
        style={{
          background: 'transparent',
          border: 0,
          padding: 0,
          borderRadius: 100,
          outline: 0,
        }}
      >
        <img
          style={{
            borderRadius: 100,
            border: '2px solid red',
            width: 48,
            height: 48,
          }}
          src={url}
          alt="user"
        />
      </Button>
      <Collapse isOpen={isOpen}>
        <span
          style={{
            color: '#f00',
            fontWeight: '700',
            margin: 4,
          }}
        >
          {content}
        </span>
        <br />
        <img
          style={{
            width: '150px',
            height: '150px',
            borderRadius: 5,
            border: '2px solid #444',
          }}
          src={url}
          alt=""
        />
      </Collapse>
    </div>
  );
};

Mark.propTypes = {
  url: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

export default Mark;
