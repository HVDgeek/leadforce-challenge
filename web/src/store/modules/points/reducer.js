const INITIAL_STATE = {
  id: '',
  activeModal: false,
  latitude: 0,
  longitude: 0,
  markers: [],
};

function points(state = INITIAL_STATE, action) {

  switch (action.type) {

    case 'ADD_POINT_SUCCESS':
      return{
        ...state,
        markers: [...state.markers, action.points]
      }

    case 'LIST_POINTS_SUCCESS':
      return {
        ...state,
        markers:  action.points
      }

    case 'OPEN_MODAL_FORM':
      return {
        ...state,
        activeModal: true,
        latitude: action.lat,
        longitude: action.long,
        id: action.id,
      };

    case 'CLOSE_MODAL_FORM':
      return {
        ...state,
        activeModal: false,
        latitude: 0,
        longitude: 0,
      };
    default:
      return state;
  }
}

export default points;
