const mongoose = require('mongoose');
const request = require('supertest');
const app = require('../src/app');

describe('Point test', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      { useNewUrlParser: true, useCreateIndex: true },
      err => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      }
    );
  });

  afterAll(done => {
    return done();
  });

  it('should be able to register point with file', async () => {
    // Create user
    const response = await request(app)
      .post('/users')
      .send({
        name: 'Hiduino Domingos',
      });

    // create point
    const response2 = await request(app)
      .post(`/users/${response.body._id}/points`)
      .attach(
        'file',
        '/home/hiduino/Documentos/lead-force/challenge/server/tmp/índice.png'
      )
      .field('latitude', '54')
      .field('longitude', '46')
      .field('content', 'Meu primeiro ponto no mapa');
    expect(response2.status).toBe(200);
  });

  it('should not be able register point with invalid user', async () => {
    // create point
    const response2 = await request(app)
      .post(`/users/6/points`)
      .attach(
        'file',
        '/home/hiduino/Documentos/lead-force/challenge/server/tmp/índice.png'
      )
      .field('latitude', '54')
      .field('longitude', '46')
      .field('content', 'Meu primeiro ponto no mapa');
    expect(response2.status).toBe(400);
  });

  it('should not be able delete point with invalid user', async () => {
    const response2 = await request(app).delete(`/users/6/points/?idPoint=6`);

    expect(response2.status).toBe(400);
  });

  it('should not be able delete point if the point not exist', async () => {
    // Create user
    const response = await request(app)
      .post('/users')
      .send({
        name: 'Hiduino Domingos',
      });

    // delete point
    const response2 = await request(app).delete(
      `/users/${response.body._id}/points/?idPoint=6`
    );

    expect(response2.status).toBe(400);
  });

  it('should be able delete point if user and point is valid', async () => {
    // Create user
    const response = await request(app)
      .post('/users')
      .send({
        name: 'Hiduino Domingos',
      });

    // create point
    const response2 = await request(app)
      .post(`/users/${response.body._id}/points`)
      .attach(
        'file',
        '/home/hiduino/Documentos/lead-force/challenge/server/tmp/índice.png'
      )
      .field('latitude', '54')
      .field('longitude', '46')
      .field('content', 'Meu primeiro ponto no mapa');

    // delete point

    const response3 = await request(app).delete(
      `/users/${response.body._id}/points/?idPoint=${response2.body._id}`
    );

    expect(response3.status).toBe(200);
  });
});
