<h1 align="center">
  <img alt="Lead Map" title="Lead map" src=".github/logo.svg" />
</h1>

**Vídeo da aplicação:** [Video](https://youtu.be/h73ytlWFjoY)

### :rocket: SOBRE

O objetivo desta aplicação é disponibilizar um mapa onde o usuário poderá adicionar marcadores composto por uma imagem e uma mensagem. O usuário deverá informar também a latitude/longitude.

### REGRAS

- Usuários poderão entrar na aplicação através do nome
- Usuários poderão adicionar marcadores no mapa
- Usuários poderão fazer upload de uma imagem e uma mensagem no marcador
- Usuários poderão visualizar as informações do marcador clicando-o
- Usuários poderão remover o marcador através da barra lateral com a lista de todos os marcadores adicionados.

### TELAS

#### LOGIN

![1](.github/1-Login.png)

#### TUTORIAL

![2](.github/2-TUTORIAL.png)

#### ADICIONAR MARCADOR

![3](.github/4-ADD.png)

#### MAIN

![4](.github/5-main.png)

#### VIEW

![4](.github/6-View.png)


***Vídeo da aplicação:*** [Video](https://youtu.be/h73ytlWFjoY)