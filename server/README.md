<h1 align="center">
  <img alt="Lead Map" title="Lead Map" src="../.github/logo.svg" />
</h1>

**Vídeo da aplicação:** (https://youtu.be/h73ytlWFjoY)

### Testes

![1](../.github/testes_backend.png)


### Instalação

Yarn

```js
yarn
```

NPM:

```js
npm install
```
### Execução

Yarn

```js
yarn dev
```

NPM:

```js
npm run dev
```

### Testes

Yarn

```js
yarn test
```

NPM:

```js
npm run test
```

### TECNOLOGIAS UTILIZADAS

- NodeJS
- MongoDB
- MongoDB Atlas
- Mongoose
- Multer
- Jest
- TDD
- WebService
- Nodemon
- Sucrase
- eslint and prettier
