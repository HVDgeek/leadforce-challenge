import React from 'react';
import {
  Button,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
} from 'reactstrap';

const Tutorial = () => {
  return (
    <div>
      <Button id="PopoverLegacy" className="btn btn-dark" type="button">
        <i className="fa fa-question-circle" /> Tutorial
      </Button>
      <UncontrolledPopover
        trigger="legacy"
        placement="bottom"
        target="PopoverLegacy"
      >
        <PopoverHeader>
          <i style={{ color: '#2ecc71' }} className="fa fa-plus-circle" />{' '}
          Adicionar um Marcador
        </PopoverHeader>
        <PopoverBody>
          Para <b>adicionar</b> um marcador clique sobre o mapa
        </PopoverBody>
        <PopoverHeader>
          <i style={{ color: '#3498db' }} className="fa fa-eye" /> Visualizar o
          conteúdo do Marcador
        </PopoverHeader>
        <PopoverBody>
          Para <b>visualizar</b> o conteúdo do Marcador clique sobre o Marcador
        </PopoverBody>
        <PopoverHeader>
          <i style={{ color: '#e67e22' }} className="fa fa-trash" /> Remover
          Marcador
        </PopoverHeader>
        <PopoverBody>
          Para <b>remover</b> marcador, clique no ícone{' '}
          <i
            style={{ color: '#f00' }}
            className="fa fa-fw fa-times-circle remove"
          />{' '}
          na barra lateral
        </PopoverBody>
      </UncontrolledPopover>
    </div>
  );
};

export default Tutorial;
