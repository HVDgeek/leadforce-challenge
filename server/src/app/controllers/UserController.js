const User = require('../models/User');

class UserController {
  async store(req, res) {
    if (!req.body.name) {
      return res.status(400).json({ error: 'Name is required' });
    }

    const user = await User.create(req.body);

    return res.json(user);
  }

  async show(req, res) {
    try {
      const user = await User.findById(req.params.id)
        .sort('-createdAt')
        .populate({
          path: 'points',
          sort: { createdAt: -1 },
        });

      return res.json(user);
    } catch (error) {
      return res.status(400).json({ error: 'User not founf' });
    }
  }
}

module.exports = new UserController();
