const mongoose = require('mongoose');

const PointSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },

    path: {
      type: String,
      required: true,
    },

    latitude: {
      type: String,
      required: true,
    },

    longitude: {
      type: String,
      required: true,
    },

    content: String,
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  }
);

PointSchema.virtual('url').get(function() {
  return `http://localhost:3333/files/${encodeURIComponent(this.path)}`;
});

module.exports = mongoose.model('Point', PointSchema);
