import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Main from './pages/Main';
import User from './pages/User';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={User} />
        <Route path="/map/:id" component={Main} />
      </Switch>
    </BrowserRouter>
  );
}
