const { resolve } = require('path');
const multer = require('multer');
const crypto = require('crypto');

module.exports = {
  storage: multer.diskStorage({
    destination: resolve(__dirname, '..', '..', 'tmp'),
    filename: (req, file, cb) => {
      crypto.randomBytes(16, (err, hash) => {
        if (err) cb(err);

        cb(null, `${hash.toString('hex')}-${file.originalname}`);
      });
    },
  }),
};
