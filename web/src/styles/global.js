import { createGlobalStyle } from 'styled-components';
import 'react-toastify/dist/ReactToastify.css';
import 'font-awesome/css/font-awesome.css';

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap');

  *{
    margin: 0;
    outline: 0;
    box-sizing: border-box;
    padding: 0;
  }

  *:focus{
    outline: 0
  }

  html, #root, body{
    height: 100%;
  }

  body{
    -webkit-font-smoothing: antialiased;

  }

  body, input, button {
    font: 14px 'Roboto', sans-serif;
    color: #333;
  }
  a {
    text-decoration: none;
  }

  ul{
    list-style: none;
  }

  button{
    cursor: pointer;
  }
`;
