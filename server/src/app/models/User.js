const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },

    points: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Point' }],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('User', UserSchema);
