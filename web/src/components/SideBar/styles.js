import styled from 'styled-components';

const Container = styled.div`
  overflow-y: auto;
  height: 75%;
  width: 22%;
  position: absolute;
  background: rgba(150, 150, 150, 0.4);
  border-radius: 5px;
  top: 14%;
  left: 2%;

  margin-bottom: 1%;

  h1 {
    font-size: 18px;
    color: #333;
    font-weight: bold;
    display: flex;
    justify-content: center;
    margin-top: 16px;
  }

  img {
    width: 40px;
    height: 40px;
    border-radius: 50%;
  }

  button {
    border: 0;
    background: transparent;
    outline: 0;
    padding: 0;
    color: red;

    &:hover {
      background: transparent;
      outline: 0;
      opacity: 0.7;
    }
    &:focus {
      outline: 0;
    }
  }

  li {
    display: flex;
    justify-content: space-between;
    align-items: center;
    &:hover {
      background: rgba(0, 0, 0, 0.2);
    }
  }

  ul {
    margin-top: 10px;
  }
  .image {
    margin: 10px;
  }

  .button {
    margin-right: 20px;
  }

  .content {
    display: flex;
    justify-content: center;
    flex-direction: column;
    text-align: center;
    .date {
      color: #666;
      font-size: 14px;
    }
  }
`;

export { Container };
