import { combineReducers } from 'redux';

import points from './points/reducer';

export default combineReducers({
  points,
});
