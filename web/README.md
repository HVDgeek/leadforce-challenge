<h1 align="center">
  <img alt="Lead Map" title="Lead Map" src="../.github/logo.svg" />
</h1>

**Vídeo da aplicação:** (https://youtu.be/h73ytlWFjoY)


### Instalação

Yarn

```js
yarn
```

NPM:

```js
npm install
```
### Execução

Yarn

```js
yarn start
```

NPM:

```js
npm run start
```

### Testes

Yarn

```js
yarn test
```

NPM:

```js
npm run test
```

### TECNOLOGIAS UTILIZADAS

- ReactJS
- React-Hooks
- Styled component
- Routes
- Redux
- Redux-Saga
- Reactotron
- axios
- Google map
- Jest
- TDD
- React-Toastify
- Reactstrap [Bootstrap]
- eslint and prettier

