// LIST
export function ListPointsRequest(id) {
  return {
    type: 'LIST_POINTS_REQUEST',
    id,
  };
}

export function ListPointsSuccess(points) {
  return {
    type: 'LIST_POINTS_SUCCESS',
    points,
  };
}

// ADD
export function AddPointRequest(id, data){
  return {
    type: 'ADD_POINT_REQUEST',
    id,
    data
  }
}

export function AddPointSuccess(points){
  return {
    type: 'ADD_POINT_SUCCESS',
    points
  }
}

// REMOVE

export function RemovePointRequest(id, idPoint) {
  return {
    type: 'REMOVE_POINT_REQUEST',
    id,
    idPoint
  };
}


// MODAL FORM
export function OpenModalForm(latitude, longitude, id) {
  return {
    type: 'OPEN_MODAL_FORM',
    lat: longitude,
    long: latitude,
    id,
  };
}

export function CloseModalForm() {
  return {
    type: 'CLOSE_MODAL_FORM',
  };
}
