import { call, put, all, takeLatest } from 'redux-saga/effects';
import api from '../../../services/api';
import {ListPointsSuccess, AddPointSuccess} from './actions'
import { toast } from 'react-toastify';

function* listPoints({ id }) {

  const response = yield call(api.get, `users/${id}`)

  yield put(ListPointsSuccess(response.data.points))

}

function* addPoints({ id, data }) {
  try {
    const response = yield call(api.post, `users/${id}/points`, data)

    yield put(AddPointSuccess(response.data))

    toast.success('Marcador adicionado com sucesso!')

  } catch (error) {
    toast.error('Erro ao adicionar marcador')
  }


}

function* removePoints({id, idPoint}){

    try {
      yield call(api.delete, `users/${id}/points?idPoint=${idPoint}`)
      const response2 = yield call(api.get, `users/${id}`)

      yield put(ListPointsSuccess(response2.data.points))
      toast.warn('Marcador Removido!')

    } catch (error) {
      toast.error('Não foi possível remover este marcador')
    }


}

export default all([
  takeLatest('LIST_POINTS_REQUEST', listPoints),
  takeLatest('ADD_POINT_REQUEST', addPoints),
  takeLatest('REMOVE_POINT_REQUEST', removePoints),
])
