const express = require('express');
const cors = require('cors');
const path = require('path');
const mongoose = require('mongoose');
const serv = require('http');
const io = require('socket.io');
const routes = require('./routes');

class App {
  constructor() {
    this.app = express();
    this.server = serv.Server(this.app);

    this.io = io(this.server);
    this.io.on('connection', socket => {
      socket.on('connectRoom', user => {
        socket.join(user);
      });
    });

    this.database();
    this.middlewares();
    this.routes();
  }

  database() {
    mongoose.connect(
      'mongodb+srv://laserca:laserca@cluster0-edi18.mongodb.net/leadforce?retryWrites=true&w=majority',
      {
        useFindAndModify: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }
    );
  }

  middlewares() {
    this.app.use((req, res, next) => {
      req.io = this.io;

      return next();
    });

    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(
      '/files',
      express.static(path.resolve(__dirname, '..', 'tmp'))
    );
  }

  routes() {
    this.app.use(routes);
  }
}

module.exports = new App().server;
