const mongoose = require('mongoose');
const request = require('supertest');
const app = require('../src/app');

describe('User test', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      { useNewUrlParser: true, useCreateIndex: true },
      err => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      }
    );
  });

  afterAll(done => {
    return done();
  });

  it('should not be able register if name not fill', async () => {
    const response = await request(app)
      .post('/users')
      .send({
        name: '',
      });

    expect(response.status).toBe(400);
  });

  it('should be able to register', async () => {
    const response = await request(app)
      .post('/users')
      .send({
        name: 'Hiduino Domingos',
      });

    expect(response.body._id).toBeDefined();
    expect(response.body.name).toBe('Hiduino Domingos');
    expect(response.status).toBe(200);
  });

  it('should be able show user with points by id', async () => {
    const response = await request(app)
      .post('/users')
      .send({
        name: 'Hiduino Domingos',
      });

    const response2 = await request(app).get(`/users/${response.body._id}`);
    expect(response2.status).toBe(200);
  });

  it('should not be able show user with points by id', async () => {
    const response2 = await request(app).get(`/users/3`);
    expect(response2.status).toBe(400);
  });
});
